-- Database: etl

-- DROP DATABASE etl;

CREATE DATABASE etl
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'C'
    LC_CTYPE = 'C'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
	
DROP TABLE IF EXISTS businesses;
DROP TABLE IF EXISTS inspections;
DROP TABLE IF EXISTS legends;
DROP TABLE IF EXISTS violations;
DROP TABLE IF EXISTS restaurant_scores;

CREATE TABLE IF NOT EXISTS legends(
	minimum_score INT NOT NULL UNIQUE,
	maximum_score INT NOT NULL UNIQUE,
	description VARCHAR(100) UNIQUE,
	PRIMARY KEY(minimum_score, maximum_score)
);
-- create multi key primary key

CREATE TABLE IF NOT EXISTS businesses(
	id SERIAL PRIMARY KEY,
	business_id INTEGER NOT NULL UNIQUE,
	name VARCHAR(50) NOT NULL UNIQUE,
	address VARCHAR(100) NOT NULL UNIQUE,
	city VARCHAR(20),
	postal_code CHAR(5),
	latitude NUMERIC,
	longitude NUMERIC,
	phone_number CHAR(11),
	tax_code CHAR (3),
	business_certificate VARCHAR(7),
	application_date DATE,
	owner_name VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS inspections(
	id serial PRIMARY KEY,
	business_id INT,
	score INT,
	date DATE,
	type VARCHAR(50)

)

CREATE TABLE IF NOT EXISTS violations(
	id serial PRIMARY KEY,
	business_id INT,
	date DATE,
	violationtypeid CHAR(6),
	risk_category VARCHAR(20),
	description VARCHAR(100)

)

-- set up constraints afer data load
-- add inspections table fk to businesses
ALTER TABLE inspections
ADD CONSTRAINT BUSINESSES_INSPECTIONS_NO_FKEY
FOREIGN KEY (business_id)
REFERENCES businesses(business_id);

-- add business_id fk to businesses
ALTER TABLE violations
ADD CONSTRAINT BUSINESSES_VIOLATIONS_NO_FKEY
FOREIGN KEY (business_id)
REFERENCES businesses(business_id);


